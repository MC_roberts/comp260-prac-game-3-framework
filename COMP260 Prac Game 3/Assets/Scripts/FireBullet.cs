﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	public double reloadtime = 5.0;
	private double reload = 10;
	void Update () {
		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1") && reload > reloadtime) {

			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;
			reload = 0.0;

		}
		reload = reload + Time.deltaTime;
	}

}
